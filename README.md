## Graylog

### Start Graylog

namespace = default

Requirements:

* Elasticsearch
* MongoDB

1 - Create secret:

```
$ kubectl -n default apply -f secret.yaml
```
note: graylog-deploy.yaml contains the value of secret.yaml

2 - Create PV

```
$ kubectl -n default apply -f pv-graylog-data.yaml
$ kubectl -n default apply -f pv-graylog-plugin.yaml
```

3 - Create PVC

```
$ kubectl -n default apply -f pvc-graylog-data.yaml
$ kubectl -n default apply -f pvc-graylog-plugin.yaml
```

* copy conf/graylog.conf in pvc from the yaml file "pvc-graylog-data.yaml".
*  If you need a plugin, add in pvc from the yaml file "pvc-graylog-plugin.yaml".

4 - Create service:

```
$ kubectl -n default apply -f graylog-service.yaml
```

5 - Create deployment:

```
$ kubectl -n default apply -f graylog-deploy.yaml
```
